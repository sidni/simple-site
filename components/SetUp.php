<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 08.01.18
 * Time: 22:57
 */

namespace app\components;


use yii\base\BootstrapInterface;
use yii\helpers\Inflector;

class SetUp implements BootstrapInterface
{
public function bootstrap($app)
{
    // TODO: Implement bootstrap() method.
    Inflector::$transliterator = 'Russian-Latin/BGN; NFKD';
}
}