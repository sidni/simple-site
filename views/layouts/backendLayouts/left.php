<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Основные Разделы', 'options' => ['class' => 'header']],
                    ['label' => 'Пользователи', 'icon' => 'user', 'url' => ['/backend/user/default']],

	                [
		                'label' => 'Инф. страницы',
		                'icon' => 'id-card-o',
		                'url' => '#',
		                'items' => [
			                ['label' => 'Cтраницы', 'icon' => 'book', 'url' => ['/backend/info-page']],
			                ['label' => 'Слайдеры', 'icon' => 'file-image-o', 'url' => ['/backend/slider']],
		                ],
	                ],
                    [
                        'label' => 'Витрина',
                        'icon' => 'shopping-cart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Категории', 'icon' => 'folder-open', 'url' => ['/backend/category']],
	                        ['label' => 'Продукты', 'icon' => 'product-hunt', 'url' => ['/backend/product']],
	                        ['label' => 'Характеристики', 'icon' => 'bars', 'url' => ['/backend/characteristic']],
	                        ['label' => 'Производители', 'icon' => 'copyright', 'url' => ['/backend/brand']],
                        ],
                    ],
                    [
                        'label' => 'Блог',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Разделы', 'icon' => 'newspaper-o', 'url' => ['/backend/blog'],],
                            ['label' => 'Статьи', 'icon' => 'file-o', 'url' => ['/backend/article'],],
                        ],
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
