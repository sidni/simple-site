<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 08.01.18
 * Time: 21:08
 */

namespace app\services\helpers;


class SeoHelper
{
    public static function seokeywords($contents,$symbol = 5,$words = 35)
    {
        $contents = @preg_replace(["'<[\/\!]*?[^<>]*?>'si","'([\r\n])[\s]+'si","'&[a-z0-9]{1,6};'si","'( +)'si"],["","\\1 "," "," "],strip_tags($contents));
        $rearray = ["~","!","@","#","$","%","^","&","*","(",")","_","+","`",'"',"№",";",":","?","-","=","|","\"","\\","/","[","]","{","}","'",",",".","<",">","\r\n","\n","\t","«","»"];
        $adjectivearray = ["ые","ое","ие","ий","ая","ый","ой","ми","ых","ее","ую","их","ым","как","для","что","или","это","этих","всех","вас","они","оно","еще","когда","где","эта","лишь","уже","вам","нет","если","надо","все","так","его","чем","при","даже","мне","есть","только","очень","сейчас","точно","обычно"];


        $contents = @str_replace($rearray," ",$contents);
        $keywordcache = @explode(" ",$contents);
        $rearray = array();

        foreach($keywordcache as $word){
            if(strlen($word)>=$symbol && !is_numeric($word)){
                $adjective = substr($word,-2);
                if(!in_array($adjective,$adjectivearray) && !in_array($word,$adjectivearray)){
                    $rearray[$word] = (array_key_exists($word,$rearray)) ? ($rearray[$word] + 1) : 1;
                }
            }
        }

        @arsort($rearray);
        $keywordcache = @array_slice($rearray,0,$words);
        $keywords = "";

        foreach($keywordcache as $word=>$count){
            $keywords.= ",".$word;
        }

        return substr($keywords,1);
    }
    public static function get_ip() {
        //Just get the headers if we can or else use the SERVER global
        if ( function_exists( 'apache_request_headers' ) ) {
            $headers = apache_request_headers();
        } else {
            $headers = $_SERVER;
        }
        //Get the forwarded IP if it exists
        if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
            $the_ip = $headers['X-Forwarded-For'];
        } elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $headers ) && filter_var($headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $the_ip = $headers['HTTP_X_FORWARDED_FOR'];
        } else {
            $the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
        }
        return $the_ip;
    }
}