<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 07.01.18
 * Time: 15:26
 */

namespace app\models\validators;


use yii\validators\RegularExpressionValidator;

class SlugValidator extends RegularExpressionValidator
{
    public $pattern = '#^[A-Za-z0-9_-]*$#s';
    public $message = 'Разрешены только символы [A-Za-z0-9_-].';
}