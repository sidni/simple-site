<?php

namespace app\models;

use app\models\generic\behaviors\MetaBehavior;
use app\models\validators\SlugValidator;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\SluggableBehavior;
use yiidreamteam\upload\ImageUploadBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use app\models\generic\Meta;
use yii\helpers\ArrayHelper;
use app\models\Blog;
use app\services\WaterMarker;

/**
 * This is the model class for table "article".
 *
 * @property int $id ID Статьи
 * @property string $title Заголовок
 * @property string $slug Сео урл
 * @property string $meta_json Мета теги
 * @property string $photo Изображение
 * @property string $content Содержание
 * @property int $status Включена
 * @property int $blog_id Раздел
 * @property int $hits Кол-во просмотров
 * @property int $created_at Создана
 * @property int $updated_at Обновлена
 * @property Meta $meta
 *
 * @property Blog $blog
 * @property PhotosForm $imageFile
 * @mixin NestedSetsBehavior
 * @mixin ImageUploadBehavior
 */
class Article extends ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;

    public $imageFile;
    public $meta;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title',  'content', ], 'required'],
            [['meta_json', 'content'], 'string'],
            [['status', 'blog_id', 'hits', 'created_at', 'updated_at'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
            [['title', 'slug'], 'string', 'max' => 250],
            [['slug'], 'unique'],
            ['slug', SlugValidator::className()],
            [['photo'], 'image'],
            [['blog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blog::className(), 'targetAttribute' => ['blog_id' => 'id']],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID Статьи',
            'title' => 'Заголовок',
            'slug' => 'Сео урл',
            'meta_json' => 'Мета теги',
            'photo' => 'Изображение',
            'content' => 'Содержание',
            'status' => 'Включена',
            'blog_id' => 'Раздел',
            'hits' => 'Кол-во просмотров',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
        ];
    }

    public static function create($title,$slug,$content,$status,$blog_id,Meta $meta)
    {
        $article = new static();
        $article->slug = $slug;
        $article->title = $title;
        $article->content = $content;
        $article->meta = $meta;
        $article->status = $status;
        $article->hits = 0;
        $article->blog_id = $blog_id;
        return $article;
    }

    public function edit($title,$slug,$content,$hits,$status,$blog_id,Meta $meta)
    {
        $this->slug = $slug;
        $this->title = $title;
        $this->content = $content;
        $this->meta = $meta;
        $this->status = $status;
        $this->hits = $hits;
        $this->blog_id = $blog_id;
    }
    public function getSeoTitle()
    {
        return $this->meta->title ?: $this->getHeadingTile();
    }

    public function getHeadingTile()
    {
        return $this->title;
    }

    public function setPhoto(UploadedFile $photo)
    {
        $this->photo = $photo;
    }
    public function behaviors()
    {
        return [
            MetaBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['blog'],
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'photo',
                'createThumbsOnRequest' => true,
                'filePath' => '@app/web/image/origin/article/[[filename]]_[[id]].[[extension]]',
                'fileUrl' => '/image/origin/article/[[filename]]_[[id]].[[extension]]',
                'thumbPath' => '@app/web/image/cache/article/[[profile]]_[[filename]]_[[id]].[[extension]]',
                'thumbUrl' => '/image/cache/article/[[profile]]_[[filename]]_[[id]].[[extension]]',
                'thumbs' => [
                    'admin' => ['width' => 100, 'height' => 70],
                    'thumb' => ['width' => 640, 'height' => 480],
                    'blog_list' => ['width' => 1000, 'height' => 150],
                    'widget_list' => ['width' => 228, 'height' => 228],
                    'origin' => ['processor' => [new WaterMarker(1024, 768, '@app/web/image/logo.png'), 'process']],
                ],
            ],
        ];

    }
    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_DISABLED => 'Отключена',
            self::STATUS_ACTIVE => 'Активна',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blog::className(), ['id' => 'blog_id']);
    }
}
