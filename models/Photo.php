<?php

namespace app\models;

use app\services\WaterMarker;
use Yii;
use yii\web\UploadedFile;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "photo".
 *
 * @property int $id Фото  ID
 * @property int $product_id Продукт
 * @property string $photo Фото
 * @property int $sort Порядок
 *
 * @property Product $product
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['id', 'product_id', 'sort'], 'integer'],
	        [['photo'], 'image'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Фото  ID',
            'product_id' => 'Продукт',
            'photo' => 'Фото',
            'sort' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
	public function behaviors()
	{
		return [
			[
				'class' => ImageUploadBehavior::className(),
				'attribute' => 'photo',
				'createThumbsOnRequest' => true,
				'filePath' => '@app/web/image/origin/products/[[attribute_product_id]]/[[attribute_photo]]',
				'fileUrl' => '/image/origin/products/[[attribute_product_id]]/[[attribute_photo]]',
				'thumbPath' => '@app/web/image/cache/products/[[attribute_product_id]]/[[profile]]_[[attribute_photo]]',
				'thumbUrl' => '/image/cache/products/[[attribute_product_id]]/[[profile]]_[[attribute_photo]]',
				'thumbs' => [
					'admin' => ['width' => 100, 'height' => 70],
					'thumb' => ['width' => 640, 'height' => 480],
					'cart_list' => ['width' => 150, 'height' => 150],
					'cart_widget_list' => ['width' => 57, 'height' => 57],
					'catalog_list' => ['width' => 228, 'height' => 228],
					'catalog_product_main' => ['processor' => [new WaterMarker(750, 1000, '@app/web/image/logo.png'), 'process']],
					'catalog_product_additional' => ['width' => 66, 'height' => 66],
					'catalog_origin' => ['processor' => [new WaterMarker(1024, 768, '@app/web/image/logo.png'), 'process']],
				],
			],
		];
	}
	public static function create(UploadedFile $file)
	{
		$photo = new static();
		$photo->photo = $file;
		return $photo;
	}
	public function setSort($sort)
	{
		$this->sort = $sort;
	}

	public function isIdEqualTo($id)
	{
		return $this->id == $id;
	}

}
