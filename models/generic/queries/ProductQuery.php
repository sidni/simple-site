<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 03.02.18
 * Time: 23:08
 */

namespace app\models\generic\queries;


use app\models\Product;
use yii\db\ActiveQuery;

class ProductQuery extends ActiveQuery
{
	/**
	 * @param null $alias
	 * @return $this
	 */
	public function active($alias = null)
	{
		return $this->andWhere([
			($alias ? $alias . '.' : '') . 'status' => Product::STATUS_ACTIVE,
		]);
	}
}