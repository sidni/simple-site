<?php

namespace app\models\generic\queries;

use paulzi\nestedsets\NestedSetsQueryTrait;
use yii\db\ActiveQuery;

class BlogQuery extends ActiveQuery
{
    use NestedSetsQueryTrait;
}