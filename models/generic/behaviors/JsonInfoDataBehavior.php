<?php

namespace app\models\generic\behaviors;

use app\models\generic\Meta;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class JsonInfoDataBehavior extends Behavior
{
    public $attribute = 'info';
    public $jsonAttribute = 'info_data';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'onAfterFind',
            ActiveRecord::EVENT_BEFORE_INSERT => 'onBeforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'onBeforeSave',
        ];
    }

    public function onAfterFind(Event $event)
    {
        $model = $event->sender;
	    $model->{$this->attribute} = Json::decode($model->getAttribute($this->jsonAttribute));
    }

    public function onBeforeSave(Event $event)
    {
        $model = $event->sender;
        $model->setAttribute($this->jsonAttribute, Json::encode($model->{$this->attribute}));
    }
}