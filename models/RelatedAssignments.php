<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "related_assignments".
 *
 * @property int $product_id Продукт
 * @property int $related_id Связанный продукт
 *
 * @property Product $product
 * @property Product $related
 */
class RelatedAssignments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'related_assignments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'related_id'], 'required'],
            [['product_id', 'related_id'], 'integer'],
            [['product_id', 'related_id'], 'unique', 'targetAttribute' => ['product_id', 'related_id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['related_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['related_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Продукт',
            'related_id' => 'Связанный продукт',
        ];
    }

	public function isForProduct($id)
	{
		return $this->related_id == $id;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelated()
    {
        return $this->hasOne(Product::className(), ['id' => 'related_id']);
    }
}
