<?php

namespace app\models;

use app\models\generic\Meta;
use app\models\generic\queries\BlogQuery;
use app\models\validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use app\modules\backend\forms\PhotosForm;
use paulzi\nestedsets\NestedSetsBehavior;
use app\models\generic\behaviors\MetaBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yiidreamteam\upload\ImageUploadBehavior;
use app\services\WaterMarker;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * This is the model class for table "blog".
 *
 * @property int $id ID Блога
 * @property string $title Заголовок
 * @property string $slug Сео урл
 * @property string $meta_json Мета теги
 * @property string $photo Изображение
 * @property string $content Содержание
 * @property int $status Статус
 * @property int $created_at Создана
 * @property int $updated_at Обновлена
 * @property int $sort Сортировка
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property int $parentId
 * @property Meta $meta
 *
 * @property Article[] $articles
 * @property Blog $parent
 * @property Blog[] $parents
 * @property Blog[] $children
 * @property Blog $prev
 * @property Blog $next
 * @property PhotosForm $imageFile
 * @mixin NestedSetsBehavior
 * @mixin ImageUploadBehavior
 */
class Blog extends ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;

    public $meta;
    public $parentId;
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug','content'], 'required'],
            [['meta_json', 'content'], 'string'],
            [['status', 'sort', 'lft', 'rgt', 'depth','parentId'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
            [['title', 'slug'], 'string', 'max' => 250],
            [['slug'], 'unique'],
            ['slug', SlugValidator::className()],
            [['photo'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID Блога',
            'title' => 'Заголовок',
            'slug' => 'Сео урл',
            'meta_json' => 'Мета теги',
            'photo' => 'Изображение',
            'parentId' => 'Основной раздел',
            'content' => 'Содержание',
            'status' => 'Статус',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
            'sort' => 'Сортировка',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
        ];
    }
    public static function create($title,$slug,$content,$sort,$status,$parentId,Meta $meta)
    {
        $blog = new static();
        $blog->slug = $slug;
        $blog->title = $title;
        $blog->content = $content;
        $blog->meta = $meta;
        $blog->sort = $sort;
        $blog->status = $status;
        $blog->parentId = $parentId;
        return $blog;
    }

    public function edit($title,$slug,$content,$sort,$status,$parentId,Meta $meta)
    {
        $this->slug = $slug;
        $this->title = $title;
        $this->content = $content;
        $this->meta = $meta;
        $this->sort = $sort;
        $this->status = $status;
        $this->parentId = $parentId;
    }

    public function setPhoto(UploadedFile $photo)
    {
        $this->photo = $photo;
    }

    public function getSeoTitle()
    {
        return $this->meta->title ?: $this->getHeadingTile();
    }

    public function getHeadingTile()
    {
        return $this->title;
    }
    public function behaviors()
    {
        return [
            MetaBehavior::className(),
            NestedSetsBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['articles'],
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'photo',
                'createThumbsOnRequest' => true,
                'filePath' => '@app/web/image/origin/blog/[[filename]]_[[id]].[[extension]]',
                'fileUrl' => '/image/origin/blog/[[filename]]_[[id]].[[extension]]',
                'thumbPath' => '@app/web/image/cache/blog/[[profile]]_[[filename]]_[[id]].[[extension]]',
                'thumbUrl' => '/image/cache/blog/[[profile]]_[[filename]]_[[id]].[[extension]]',
                'thumbs' => [
                    'admin' => ['width' => 100, 'height' => 70],
                    'thumb' => ['width' => 640, 'height' => 480],
                    'blog_list' => ['width' => 1000, 'height' => 150],
                    'widget_list' => ['width' => 228, 'height' => 228],
                    'origin' => ['processor' => [new WaterMarker(1024, 768, '@app/web/image/logo.png'), 'process']],
                ],
            ],
        ];

    }
    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_DISABLED => 'Отключена',
            self::STATUS_ACTIVE => 'Активна',
        ];
    }
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    public function parentBlogList()
    {
        return ArrayHelper::map(Blog::find()->orderBy('lft')->asArray()->all(), 'id', function (array $blog) {
            return ($blog['depth'] > 1 ? str_repeat('-- ', $blog['depth'] - 1) . ' ' : '') . $blog['title'];
        });
    }
    public static function blogList()
    {
        return ArrayHelper::map(Blog::find()->orderBy('lft')->where(['>','depth',0])->asArray()->all(), 'id', function (array $blog) {
            return ($blog['depth'] > 1 ? str_repeat('-- ', $blog['depth'] - 1) . ' ' : '') . $blog['title'];
        });
    }
    public static function find()
    {
        return new BlogQuery(get_called_class());
    }
    public function getRoot()
    {
        return static::find()->roots()->one();
    }
    /**
     * @return Category[]
     */
    public function getAll()
    {
        return static::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->all();
    }

    /*public function find($id)
    {
        return static::find()->andWhere(['id' => $id])->andWhere(['>', 'depth', 0])->one();
    }/**/

    public function findBySlug($slug)
    {
        return static::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }

    public function getTreeWithSubsOf(Blog $blog = null)
    {
        $query = static::find()->andWhere(['>', 'depth', 0])->orderBy('lft');

        if ($blog) {
            $criteria = ['or', ['depth' => 1]];
            foreach (ArrayHelper::merge([$blog], $blog->parents) as $item) {
                $criteria[] = ['and', ['>', 'lft', $item->lft], ['<', 'rgt', $item->rgt], ['depth' => $item->depth + 1]];
            }
            $query->andWhere($criteria);
        } else {
            $query->andWhere(['depth' => 1]);
        }
        return $query->all();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['blog_id' => 'id']);
    }
}
