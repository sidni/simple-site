<?php

namespace app\models;

use app\models\generic\Meta;
use app\models\generic\queries\CategoryQuery;
use app\models\validators\SlugValidator;
use paulzi\nestedsets\NestedSetsBehavior;
use app\models\generic\behaviors\MetaBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\modules\backend\forms\PhotosForm;
use yii\web\UploadedFile;
use yiidreamteam\upload\ImageUploadBehavior;
use app\services\WaterMarker;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $meta_json
 * @property string $photo
 * @property int $status
 * @property int $on_main
 * @property int $sort
 * @property integer $created_at
 * @property integer $updated_at
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property int $parentId
 * @property Meta $meta
 *
 * @property Category $parent
 * @property Category[] $parents
 * @property Category[] $children
 * @property Category $prev
 * @property Category $next
 * @property PhotosForm $imageFile
 * @mixin NestedSetsBehavior
 * @mixin ImageUploadBehavior
 *
 * @property CategoryAssignments[] $categoryAssignments
 * @property Product[] $products
 */
class Category extends ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;

    const STATUS_ON_MAIN = 1;
    const STATUS_OFF_MAIN = 0;

    public $meta;
    public $parentId;
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',  'description'], 'required'],
            [['description', 'meta_json'], 'string'],
            [['status', 'on_main', 'sort', 'lft', 'rgt', 'depth','parentId'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
            [['name', 'slug', 'title'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['photo'], 'image'],
            ['slug', SlugValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идетификатор',
            'name' => 'Название',
            'slug' => 'Сео урл',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'meta_json' => 'Мета теги',
            'parentId' => 'Основная категория',
            'status' => 'Статус',
            'photo' => 'Изображение',
            'on_main' => 'На главной',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'sort' => 'Сортировка',
            'lft' => 'Лево',
            'rgt' => 'Право',
            'depth' => 'Глубина',
        ];
    }
    public static function create($name, $slug, $title, $description,$on_main,$sort,$status,$parentId,Meta $meta)
    {
        $category = new static();
        $category->name = $name;
        $category->slug = $slug;
        $category->title = $title;
        $category->description = $description;
        $category->meta = $meta;
        $category->on_main = $on_main;
        $category->sort = $sort;
        $category->status = $status;
        $category->parentId = $parentId;
        return $category;
    }

    public function edit($name, $slug, $title, $description,$on_main,$sort,$status,$parentId,Meta $meta)
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->title = $title;
        $this->description = $description;
        $this->meta = $meta;
        $this->on_main = $on_main;
        $this->sort = $sort;
        $this->status = $status;
        $this->parentId = $parentId;
    }

    public function getSeoTitle()
    {
        return $this->meta->title ?: $this->getHeadingTile();
    }

    public function setPhoto(UploadedFile $photo)
    {
        $this->photo = $photo;
    }

    public function getHeadingTile()
    {
        return $this->title ?: $this->name;
    }
    public function behaviors()
    {
        return [
            MetaBehavior::className(),
            NestedSetsBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'photo',
                'createThumbsOnRequest' => true,
                'filePath' => '@app/web/image/origin/category/[[filename]]_[[id]].[[extension]]',
                'fileUrl' => '/image/origin/category/[[filename]]_[[id]].[[extension]]',
                'thumbPath' => '@app/web/image/cache/category/[[profile]]_[[filename]]_[[id]].[[extension]]',
                'thumbUrl' => '/image/cache/category/[[profile]]_[[filename]]_[[id]].[[extension]]',
                'thumbs' => [
                    'admin' => ['width' => 100, 'height' => 70],
                    'thumb' => ['width' => 640, 'height' => 480],
                    'blog_list' => ['width' => 1000, 'height' => 150],
                    'widget_list' => ['width' => 228, 'height' => 228],
                    'origin' => ['processor' => [new WaterMarker(1024, 768, '@app/web/image/logo.png'), 'process']],
                ],
            ],
        ];

    }
    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_DISABLED => 'Отключена',
            self::STATUS_ACTIVE => 'Активна',
        ];
    }
    public function getStatusDisplay()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesDisplayArray()
    {
        return [
            self::STATUS_OFF_MAIN => 'Скрыть',
            self::STATUS_ON_MAIN => 'Показать',
        ];
    }
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    public function parentCategoriesList()
    {
        return ArrayHelper::map(Category::find()->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
            return ($category['depth'] > 1 ? str_repeat('-- ', $category['depth'] - 1) . ' ' : '') . $category['name'];
        });
    }
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
    public function getRoot()
    {
        return static::find()->roots()->one();
    }
    /**
     * @return Category[]
     */
    public function getAll()
    {
        return static::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->all();
    }

    /*public function find($id)
    {
        return static::find()->andWhere(['id' => $id])->andWhere(['>', 'depth', 0])->one();
    }/**/

    public function findBySlug($slug)
    {
        return static::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }

    public function getTreeWithSubsOf(Category $category = null)
    {
        $query = static::find()->andWhere(['>', 'depth', 0])->orderBy('lft');

        if ($category) {
            $criteria = ['or', ['depth' => 1]];
            foreach (ArrayHelper::merge([$category], $category->parents) as $item) {
                $criteria[] = ['and', ['>', 'lft', $item->lft], ['<', 'rgt', $item->rgt], ['depth' => $item->depth + 1]];
            }
            $query->andWhere($criteria);
        } else {
            $query->andWhere(['depth' => 1]);
        }
        return $query->all();
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategoryAssignments()
	{
		return $this->hasMany(CategoryAssignments::className(), ['category_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProducts()
	{
		return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('category_assignments', ['category_id' => 'id']);
	}
}
