<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 04.02.18
 * Time: 11:48
 */

namespace app\models\helpers;

use app\models\Characteristic;
use yii\helpers\ArrayHelper;

class CharacteristicHelper {

	public static function typeList()
	{
		return [
			Characteristic::TYPE_STRING => 'String',
			Characteristic::TYPE_INTEGER => 'Integer number',
			Characteristic::TYPE_FLOAT => 'Float number',
		];
	}

	public static function typeName($type): string
	{
		return ArrayHelper::getValue(self::typeList(), $type);
	}
}