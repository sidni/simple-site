<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yiidreamteam\upload\ImageUploadBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "slider".
 *
 * @property int $id ID слайдера
 * @property string $comment Комментарии
 * @property string $photo Изображение
 * @property string $link Ссылка
 * @property string $name Название
 *
 * @property PhotosForm $imageFile
 *
 */
class Slider extends \yii\db\ActiveRecord
{
	public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'link'], 'string', 'max' => 255],
	        [['comment', 'link'], 'required'],
	        [['photo'], 'image'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID слайдера',
            'comment' => 'Комментарии',
            'photo' => 'Изображение',
            'link' => 'Ссылка',
            'name' => 'Название',
        ];
    }

	public function setPhoto(UploadedFile $photo)
	{
		$this->photo = $photo;
	}
	public function behaviors()
	{
		return [
			[
				'class' => ImageUploadBehavior::className(),
				'attribute' => 'photo',
				'createThumbsOnRequest' => true,
				'filePath' => '@app/web/image/origin/slider/[[filename]]_[[id]].[[extension]]',
				'fileUrl' => '/image/origin/slider/[[filename]]_[[id]].[[extension]]',
				'thumbPath' => '@app/web/image/cache/slider/[[profile]]_[[filename]]_[[id]].[[extension]]',
				'thumbUrl' => '/image/cache/slider/[[profile]]_[[filename]]_[[id]].[[extension]]',
				'thumbs' => [
					'admin' => ['width' => 100, 'height' => 70],
					'thumb' => ['width' => 640, 'height' => 480],
				],
			],
		];

	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getInfoPages()
	{
		return $this->hasMany(InfoPage::className(), ['slider_name' => 'name']);
	}

	/**
	 * @return array
	 */
	public static function getSliderNames()
	{
		return array_merge(['' => 'Нет'], ArrayHelper::map(self::find()->select('name')->distinct()->all(),'name','name') );
	}
}
