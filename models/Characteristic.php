<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use app\models\helpers\CharacteristicHelper;

/**
 * This is the model class for table "characteristic".
 *
 * @property int $id Характеристика ID
 * @property string $name Название
 * @property string $type Тип
 * @property int $required Обязательно
 * @property string $default Значение по умолчанию
 * @property string $variants_json Варианты
 * @property int $sort Порядок
 * @property array $variants Варианты
 *
 * @property CharacteristicValue[] $characteristicValues
 * @property Product[] $products
 * @property string $textVariants Варианты
 *
 */
class Characteristic extends ActiveRecord
{
	const TYPE_STRING = 'string';
	const TYPE_INTEGER = 'integer';
	const TYPE_FLOAT = 'float';

	public $variants;
	public $textVariants='';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'characteristic';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['name', 'type', 'sort'], 'required'],
			[['required'], 'boolean'],
			[['name', 'default'], 'string', 'max' => 255],
			[['type'], 'string', 'max' => 16],
			[['textVariants'], 'string'],
			[['sort'], 'integer'],
			//[['name'], 'unique', 'targetClass' => Characteristic::class, 'filter' => $this->_characteristic ? ['<>', 'id', $this->_characteristic->id] : null]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'Характеристика ID',
			'name' => 'Название',
			'type' => 'Тип',
			'required' => 'Обязательно',
			'default' => 'Значение по умолчанию',
			'variants_json' => 'Варианты',
			'textVariants' => 'Варианты',
			'variants' => 'Варианты',
			'sort' => 'Порядок',
		];
	}
	public static function create($name, $type, $required, $default, array $variants, $sort)
	{
		$object = new static();
		$object->name = $name;
		$object->type = $type;
		$object->required = $required;
		$object->default = $default;
		$object->variants = $variants;
		$object->sort = $sort;
		return $object;
	}

	public function edit($name, $type, $required, $default, array $variants, $sort)
	{
		$this->name = $name;
		$this->type = $type;
		$this->required = $required;
		$this->default = $default;
		$this->variants = $variants;
		$this->sort = $sort;
	}
	public function isString()
	{
		return $this->type === self::TYPE_STRING;
	}

	public function isInteger()
	{
		return $this->type === self::TYPE_INTEGER;
	}

	public function isFloat()
	{
		return $this->type === self::TYPE_FLOAT;
	}

	public function isSelect()
	{
		return count($this->variants) > 0;
	}

	public function afterFind()
	{
		$this->variants = array_filter(Json::decode($this->getAttribute('variants_json')));
		$this->textVariants = implode(PHP_EOL, $this->variants);
		parent::afterFind();
	}

	public function beforeSave($insert)
	{
		$this->setAttribute('variants_json', Json::encode(array_filter($this->variants)));
		return parent::beforeSave($insert);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristicValues()
    {
        return $this->hasMany(CharacteristicValue::className(), ['characteristic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('characteristic_value', ['characteristic_id' => 'id']);
    }

	public function typesList()
	{
		return CharacteristicHelper::typeList();
	}

	public function getVariants()
	{
		return preg_split('#\s+#i', implode(PHP_EOL, $this->variants));
	}
}
