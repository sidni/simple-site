<?php

namespace app\models;

use app\models\generic\Meta;
use app\models\validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use app\modules\backend\forms\PhotosForm;
use app\models\generic\behaviors\MetaBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yiidreamteam\upload\ImageUploadBehavior;
use app\services\WaterMarker;

/**
 * This is the model class for table "brand".
 *
 * @property int $id Бренд ID
 * @property string $name Название
 * @property string $slug Сео урл
 * @property string $meta_json Метатеги
 * @property int $created_at Создан
 * @property int $updated_at Обновлен
 * @property string $photo Фото
 * @property string $description Описание
 * @property int $on_main На главной
 *
 * @property PhotosForm $imageFile Фото
 * @property Meta $meta Метатеги
 */
class Brand extends ActiveRecord
{
	public $meta;
	public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['meta_json', 'description'], 'string'],
            [['created_at', 'updated_at', 'on_main'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
	        ['slug', SlugValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Бренд ID',
            'name' => 'Название',
            'slug' => 'Сео урл',
            'meta_json' => 'Метатеги',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'photo' => 'Фото',
            'description' => 'Описание',
            'on_main' => 'На главной',
        ];
    }
	public static function create($name,$slug,$description,$on_main=0,Meta $meta)
	{
		$brand = new static();
		$brand->slug = $slug;
		$brand->name = $name;
		$brand->description = $description;
		$brand->meta = $meta;
		$brand->on_main = $on_main;
		return $brand;
	}

	public function edit($name,$slug,$description,$on_main=0,Meta $meta)
	{
		$this->slug = $slug;
		$this->name = $name;
		$this->description = $description;
		$this->meta = $meta;
		$this->on_main = $on_main;
	}
	public function setPhoto(UploadedFile $photo)
	{
		$this->photo = $photo;
	}

	public function getSeoTitle()
	{
		return $this->meta->title ?: $this->getHeadingTile();
	}

	public function getHeadingTile()
	{
		return $this->name;
	}
	public function behaviors()
	{
		return [
			MetaBehavior::className(),
			TimestampBehavior::className(),
			[
				'class' => SluggableBehavior::className(),
				'attribute' => 'name',
			],
			[
				'class' => ImageUploadBehavior::className(),
				'attribute' => 'photo',
				'createThumbsOnRequest' => true,
				'filePath' => '@app/web/image/origin/brand/[[filename]]_[[id]].[[extension]]',
				'fileUrl' => '/image/origin/brand/[[filename]]_[[id]].[[extension]]',
				'thumbPath' => '@app/web/image/cache/brand/[[profile]]_[[filename]]_[[id]].[[extension]]',
				'thumbUrl' => '/image/cache/brand/[[profile]]_[[filename]]_[[id]].[[extension]]',
				'thumbs' => [
					'admin' => ['width' => 100, 'height' => 70],
					'thumb' => ['width' => 640, 'height' => 480],
					'blog_list' => ['width' => 1000, 'height' => 150],
					'widget_list' => ['width' => 228, 'height' => 228],
					'origin' => ['processor' => [new WaterMarker(1024, 768, '@app/web/image/logo.png'), 'process']],
				],
			],
		];

	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProducts()
	{
		return $this->hasMany(Product::className(), ['id' => 'brand_id']);
	}
}
