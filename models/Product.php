<?php

namespace app\models;

use app\models\generic\behaviors\MetaBehavior;
use app\models\generic\queries\ProductQuery;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $category_id Категория
 * @property int $brand_id Производитель
 * @property string $code Код
 * @property string $name Название
 * @property string $description Описание
 * @property int $price_old Старая цена
 * @property int $price_new Новая цена
 * @property string $rating Рейтинг
 * @property string $meta_json Метатеги
 * @property int $main_photo_id Главное фото
 * @property int $status Статус
 * @property int $weight Вес
 * @property int $quantity Количество
 * @property int $created_at Создан
 * @property int $updated_at Обновлен
 * @property string $slug Сео Урл
 * @property CategoryAssignments[] $categoryAssignments Дополнительные категории
 * @property Category[] $category
 * @property CharacteristicValue[] $characteristicValues
 * @property Characteristic[] $characteristics
 * @property Photo[] $photos
 * @property Brand $brand
 * @property RelatedAssignments[] $relatedAssignments
 * @property Product[] $relateds
 * @property Product[] $products
 */
class Product extends \yii\db\ActiveRecord
{
	use SaveRelationsTrait;

	const STATUS_DRAFT = 0;
	const STATUS_ACTIVE = 1;

	public $imageFile;
	public $meta;

	public $characteristicForms;
	public $categoryAssignmentsValues;
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'product';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['category_id', 'brand_id', 'code', 'name'], 'required'],
			[['category_id', 'brand_id', 'price_old', 'price_new', 'main_photo_id', 'status', 'weight', 'quantity', 'created_at', 'updated_at'], 'integer'],
			[['description','slug'], 'string'],
			[['rating'], 'number'],
			[['categoryAssignmentsValues'], 'each', 'rule' => ['integer']],
			['status', 'default', 'value' => self::STATUS_ACTIVE],
			['status', 'in', 'range' => array_keys(self::getStatusesArray())],
			[['code', 'name'], 'string', 'max' => 255],
			[['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
			[['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'category_id' => 'Категория',
			'brand_id' => 'Производитель',
			'code' => 'Код',
			'name' => 'Название',
			'slug' => 'Сео Урл',
			'description' => 'Описание',
			'price_old' => 'Старая цена',
			'price_new' => 'Новая цена',
			'rating' => 'Рейтинг',
			'meta_json' => 'Метатеги',
			'main_photo_id' => 'Главное фото',
			'status' => 'Статус',
			'weight' => 'Вес',
			'quantity' => 'Количество',
			'created_at' => 'Создан',
			'updated_at' => 'Обновлен',
			'categoryAssignments' => 'Дополнительные категории',
			'categoryAssignmentsValues' => 'Дополнительные категории',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategoryAssignments()
	{
		return $this->hasMany(CategoryAssignments::className(), ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategories()
	{
		return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_assignments', ['product_id' => 'id']);
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory()
	{
		return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCharacteristicValues()
	{
		return $this->hasMany(CharacteristicValue::className(), ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCharacteristics()
	{
		//return $this->hasMany(Characteristic::className(), ['id' => 'characteristic_id'])->viaTable('characteristic_value', ['product_id' => 'id']);
		return $this->hasMany(Characteristic::className(), ['id' => 'characteristic_id'])->via('characteristicValues');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPhotos()
	{
		return $this->hasMany(Photo::className(), ['product_id' => 'id'])->orderBy('sort');
	}

	public function getMainPhoto()
	{
		return $this->hasOne(Photo::className(), ['id' => 'main_photo_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBrand()
	{
		return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRelatedAssignments()
	{
		return $this->hasMany(RelatedAssignments::className(), ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRelateds()
	{
		return $this->hasMany(Product::className(), ['id' => 'related_id'])->viaTable('related_assignments', ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProducts()
	{
		return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('related_assignments', ['related_id' => 'id']);
	}

	public function brandsList()
	{
		return ArrayHelper::map(Brand::find()->orderBy('name')->asArray()->all(), 'id', 'name');
	}
	public function behaviors()
	{
		return [
			MetaBehavior::className(),
			[
				'class' => SluggableBehavior::className(),
				'attribute' => 'name',
			],
			TimestampBehavior::className(),
			[
				'class' => SaveRelationsBehavior::className(),
				'relations' => ['categoryAssignments', 'relatedAssignments', 'characteristicValues','characteristics', 'photos'],
			],
		];
	}
	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_ALL,
		];
	}
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			foreach ($this->photos as $photo) {
				$photo->delete();
			}
			return true;
		}
		return false;
	}
	public function afterSave($insert, $changedAttributes)
	{
		$related = $this->getRelatedRecords();
		parent::afterSave($insert, $changedAttributes);
		if (array_key_exists('mainPhoto', $related)) {
			$this->updateAttributes(['main_photo_id' => $related['mainPhoto'] ? $related['mainPhoto']->id : null]);
		}
	}
	public static function find()
	{
		return new ProductQuery(static::className());
	}

	public function categoriesList()
	{
		return ArrayHelper::map(Category::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
			return ($category['depth'] > 1 ? str_repeat('-- ', $category['depth'] - 1) . ' ' : '') . $category['name'];
		});
	}
	public function getStatusName()
	{
		return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
	}

	public static function getStatusesArray()
	{
		return [
			self::STATUS_DRAFT => 'Черновик',
			self::STATUS_ACTIVE => 'Активна',
		];
	}
	public function addPhoto(UploadedFile $file)
	{
		$photos = $this->photos;
		$photos[] = Photo::create($file);
		$this->updatePhotos($photos);
	}
	private function updatePhotos(array $photos)
	{
		foreach ($photos as $i => $photo) {
			$photo->setSort($i);

		}
		$this->photos = $photos;
		$this->populateRelation('mainPhoto', reset($photos));
	}
	public function removePhoto($id)
	{
		$photos = $this->photos;
		foreach ($photos as $i => $photo) {
			if ($photo->isIdEqualTo($id)) {
				unset($photos[$i]);
				$this->updatePhotos($photos);
				return;
			}
		}
		throw new \DomainException('Photo is not found.');
	}

	public function removePhotos()
	{
		$this->updatePhotos([]);
	}

	public function movePhotoUp($id)
	{
		$photos = $this->photos;
		foreach ($photos as $i => $photo) {
			if ($photo->isIdEqualTo($id)) {
				if ($prev = $photos[$i - 1] ?? null) {
					$photos[$i - 1] = $photo;
					$photos[$i] = $prev;
					$this->updatePhotos($photos);
				}
				return;
			}
		}
		throw new \DomainException('Photo is not found.');
	}
	public function movePhotoDown($id)
	{
		$photos = $this->photos;
		foreach ($photos as $i => $photo) {
			if ($photo->isIdEqualTo($id)) {
				if ($next = $photos[$i + 1] ?? null) {
					$photos[$i] = $next;
					$photos[$i + 1] = $photo;
					$this->updatePhotos($photos);
				}
				return;
			}
		}
		throw new \DomainException('Photo is not found.');
	}
	public function setValue($id, $value)
	{
		$values = $this->characteristicValues;
		foreach ($values as $val) {
			if ($val->isForCharacteristic($id)) {
				$val->change($value);
				$this->values = $values;
				return;
			}
		}
		$values[] = CharacteristicValue::create($id, $value);
		$this->values = $values;
	}

	public function getValue($id)
	{
		$values = $this->characteristicValues;
		foreach ($values as $val) {
			if ($val->isForCharacteristic($id)) {
				return $val;
			}
		}
		return CharacteristicValue::blank($id);
	}

}