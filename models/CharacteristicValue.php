<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "characteristic_value".
 *
 * @property int $product_id Продукт
 * @property int $characteristic_id Характеристика
 * @property string $value Значение
 *
 * @property Characteristic $characteristic
 * @property Product $product
 */
class CharacteristicValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'characteristic_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['product_id', 'characteristic_id'], 'required'],
            [['product_id', 'characteristic_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            //[['product_id', 'characteristic_id'], 'unique', 'targetAttribute' => ['product_id', 'characteristic_id']],
            //[['characteristic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Characteristic::className(), 'targetAttribute' => ['characteristic_id' => 'id']],
            //[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Продукт',
            'characteristic_id' => 'Характеристика',
            'value' => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristic()
    {
        return $this->hasOne(Characteristic::className(), ['id' => 'characteristic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
	public static function create($characteristicId, $value)
	{
		$object = new static();
		$object->characteristic_id = $characteristicId;
		$object->value = $value;
		return $object;
	}

	public static function blank($characteristicId)
	{
		$object = new static();
		$object->characteristic_id = $characteristicId;
		return $object;
	}

	public function change($value)
	{
		$this->value = $value;
	}

	public function isForCharacteristic($id)
	{
		return $this->characteristic_id == $id;
	}

}
