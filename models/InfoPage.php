<?php

namespace app\models;

use app\models\generic\behaviors\JsonInfoDataBehavior;
use Yii;
use app\models\generic\behaviors\MetaBehavior;
use app\models\validators\SlugValidator;
use yii\behaviors\SluggableBehavior;
use app\models\generic\Meta;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "info_page".
 *
 * @property int $id ID Страница
 * @property string $name Название
 * @property string $title Заголовок
 * @property string $slug Сео Урл
 * @property string $meta_json Мета теги
 * @property string $top_content Текст до контента
 * @property string $description Описание
 * @property int $status Статус
 * @property int $sort Сортировка
 * @property int $created_at Создана
 * @property int $updated_at Обновлена
 * @property int $sys_id Системный индетификатор
 * @property string $slider_name Cлайдер
 * @property string $info_data Дополнительная информация
 *
 * @property Meta $meta
 *
 */
class InfoPage extends \yii\db\ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;


    const PAGE_OTHER = 0;
    const PAGE_MAIN = 1;
    const PAGE_CONTACT = 2;
    const PAGE_ABOUT = 3;

    public $meta;
	public $info;

	public $map = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'info_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_json', 'top_content', 'description'], 'string'],
            [['top_content', 'description'], 'required'],
            [['status', 'sort', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 150],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
            ['slug', SlugValidator::className()],
            [['title', 'slug'], 'string', 'max' => 250],
            [['slug'], 'unique'],
	        [['slider_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID Страница',
            'name' => 'Название',
            'title' => 'Заголовок',
            'slug' => 'Сео Урл',
            'meta_json' => 'Мета теги',
            'top_content' => 'Текст до контента',
            'description' => 'Описание',
            'status' => 'Статус',
            'sort' => 'Сортировка',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
            'sys_id' => 'Системный индетификатор',
			'slider_name' => 'Слайдер',
	        'info_data' => 'Дополнительная информация'
        ];
    }
    public static function create($name,$title,$slug,$description,$top_content,$sort,$slider_name,$status,$info_data,Meta $meta)
    {
        $infoPage = new static();
	    $infoPage->name = $name;
	    $infoPage->title = $title;
	    $infoPage->slug = $slug;
	    $infoPage->title = $title;
	    $infoPage->top_content = $top_content;
	    $infoPage->meta = $meta;
	    $infoPage->status = $status;
	    $infoPage->description = $description;
	    $infoPage->sort = $sort;
	    $infoPage->slider_name = $slider_name;
	    $infoPage->sys_id = 0;
	    $infoPage->info_data = $info_data;
        return $infoPage;
    }

    public function edit($name,$title,$slug,$description,$top_content,$sort,$slider_name,$status,$info_data,Meta $meta)
    {
        $this->name = $name;
        $this->title = $title;
        $this->slug = $slug;
        $this->title = $title;
        $this->top_content = $top_content;
        $this->meta = $meta;
        $this->status = $status;
        $this->description = $description;
	    $this->slider_name = $slider_name;
        $this->sort = $sort;
	    $this->info_data = $info_data;
    }
    public function getSeoTitle()
    {
        return $this->meta->title ?: $this->getHeadingTile();
    }

    public function getHeadingTile()
    {
        return $this->title;
    }
    public function behaviors()
    {
        return [
            MetaBehavior::className(),
	        JsonInfoDataBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];

    }
    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_DISABLED => 'Отключена',
            self::STATUS_ACTIVE => 'Активна',
        ];
    }
    public function isSysNameOther()
    {
        if($this->sys_id === self::PAGE_OTHER){
            return true;
        }
        return false;
    }
	/**
	 * @return \yii\db\ActiveQuery
	 */
    public function getSlider()
    {
	    return $this->hasOne(Slider::className(), ['name' => 'slider_name']);
    }
}
