<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_assignments".
 *
 * @property int $product_id Продукт
 * @property int $category_id Связанные категории
 *
 * @property Category $category
 * @property Product $product
 */
class CategoryAssignments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_assignments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['product_id', 'category_id'], 'required'],
            [['product_id', 'category_id'], 'integer'],
            //[['product_id', 'category_id'], 'unique', 'targetAttribute' => ['product_id', 'category_id']],
            //[['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            //[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Продукт',
            'category_id' => 'Связанные категории',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
