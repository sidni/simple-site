<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 21.12.17
 * Time: 23:12
 */

return [
    'backend' => [
        'class' => 'app\modules\backend\Module',
        'layout' => '@app/views/layouts/backend',
        'modules' => [
            'user' => [
                'class' => 'app\modules\user\Module',
                'controllerNamespace' => 'app\modules\user\controllers\backend',
                'viewPath' => '@app/modules/user/views/backend',
            ],
        ]
    ],
    'frontend' => [
        'class' => 'app\modules\frontend\Module',
        'layout' => '@app/views/layouts/frontend',
    ],
    'user' => [
        'class' => 'app\modules\user\Module',
        'layout' => '@app/views/layouts/frontend',
        'controllerNamespace' => 'app\modules\user\controllers\frontend',
        'viewPath' => '@app/modules/user/views/frontend',
    ],
];