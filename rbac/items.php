<?php
return [
    'permAdminPanel' => [
        'type' => 2,
        'description' => 'Admin panel',
    ],
    'user' => [
        'type' => 1,
        'description' => 'Клиент',
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Менеджер',
        'children' => [
            'user',
            'permAdminPanel',
        ],
    ],
    'topManager' => [
        'type' => 1,
        'description' => 'Топ Менеджер',
        'children' => [
            'manager',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администратор',
        'children' => [
            'topManager',
        ],
    ],
];
