var map;
var infoWnd;
function initMap(){
    map=new google.maps.Map(
        document.getElementById('mapc'),{
            center: centerMap,
            zoom: 16,
            mapTypeControl: false,
            streetViewControl: false,
        });
    var marker = new google.maps.Marker({
        position: centerMap,
        draggable:true,
        animation: google.maps.Animation.DROP,
        map: map,
        icon: '/image/geo.png',
        title: 'Укажите Ваше место расположения'
    });

    google.maps.event.addListener(map, 'click', function(event){
        if (marker.getAnimation() !==null){
            marker.setAnimation(null);
        }
    });

    google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng;
        $("#latitude").val(latLng.lat());
        $("#longitude").val(latLng.lng());
    });
    marker.setAnimation(google.maps.Animation.BOUNCE);
}