<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 16.01.18
 * Time: 23:24
 */
use app\assets\SliderAsset;

SliderAsset::register($this);
?>

<div id="main-slider" class="carousel slide">
	<ol class="carousel-indicators">
		<li data-target="#main-slider" data-slide-to="0" class="active"></li>
		<li data-target="#main-slider" data-slide-to="1"></li>
		<li data-target="#main-slider" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="item active">
			<img src="http://placehold.it/960x405" alt="" />
            <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>LA is always so much fun!</p>
            </div>
		</div>
		<div class="item">
			<img src="http://placehold.it/960x405" alt="" />
            <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>LA is always so much fun!</p>
            </div>
		</div>
		<div class="item">
			<img src="http://placehold.it/960x405" alt="" />
            <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>LA is always so much fun!</p>
            </div>
		</div>
	</div>

	<a class="left carousel-control" href="#main-slider" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#main-slider" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<?php $this->registerJs("
    $(document).ready(function() {
        $('#main-slider').carousel({
            interval: 6000
        })
    });
"); ?>
