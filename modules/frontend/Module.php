<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 21.12.17
 * Time: 23:25
 */


namespace app\modules\frontend;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\frontend\controllers';

}