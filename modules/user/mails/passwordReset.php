<?php

use app\modules\frontend\Module;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/password-reset', 'token' => $user->password_reset_token]);
?>

<?= 'Имя: '. $user->username ?>

Чтобы сбросить пароль перейдите по ссылке

<?= $resetLink ?>