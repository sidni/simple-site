<?php

use app\modules\user\Module;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/email-confirm', 'token' => $user->email_confirm_token]);
?>

<?= 'Привет '. $user->username; ?>

Перейдите по ссылке чтоб подтвердить Email

<?= $confirmLink ?>

Игнорируйте, если Вы считаете что это письмо пришло не Вам.