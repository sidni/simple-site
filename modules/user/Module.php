<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 21.12.17
 * Time: 23:27
 */


namespace app\modules\user;

use Yii;

class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $defaultRole = 'user';
    /**
     * @var int
     */
    public $emailConfirmTokenExpire = 259200; // 3 days
    /**
     * @var int
     */
    public $passwordResetTokenExpire = 3600;

}