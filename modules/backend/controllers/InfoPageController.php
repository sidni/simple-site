<?php

namespace app\modules\backend\controllers;

use app\modules\backend\forms\MapForm;
use Yii;
use app\models\InfoPage;
use app\modules\backend\models\InfoPageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\backend\forms\MetaForm;

/**
 * InfoPageController implements the CRUD actions for InfoPage model.
 */
class InfoPageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfoPage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InfoPageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InfoPage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InfoPage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InfoPage();
        $model->meta = new MetaForm();

	    //var_dump(Yii::$app->params['googleMapKey']);
	    if($model->sys_id === InfoPage::PAGE_CONTACT){
		    $map = isset($model->info['map'])?$model->info['map']:null;
		    $model->map = new MapForm($map);
	    }

        if ($model->load(Yii::$app->request->post()) && $model->meta->load(Yii::$app->request->post()) ) {
	        if($model->sys_id === InfoPage::PAGE_CONTACT ){
		        $model->map->load(Yii::$app->request->post());
		        if($model->map->validate()){
			        $model->info['data'] = $model->map->map;
		        }
	        }
            $model->save();
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing InfoPage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->meta = new MetaForm($model->meta);

	    if($model->sys_id === InfoPage::PAGE_CONTACT){
	    	/*var_dump($model->info);
	    	die("\n");/**/
		    $map = isset($model->info['map'])?$model->info['map']:null;
		    $model->map = new MapForm($map);
	    }

        if ($model->load(Yii::$app->request->post()) && $model->meta->load(Yii::$app->request->post()) ) {
	        if($model->sys_id === InfoPage::PAGE_CONTACT ){
		        $model->map->load(Yii::$app->request->post());
		        if($model->map->validate()){
			        $model->info['map'] = $model->map->map;
		        }
	        }
            $model->save();
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing InfoPage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->isSysNameOther()){
            $model->delete();
        }else{
            Yii::$app->getSession()->setFlash('error', '<b>Внимание!</b> Нельзя удалять системные страницы.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the InfoPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InfoPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InfoPage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
