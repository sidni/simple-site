<?php

namespace app\modules\backend\controllers;

use app\modules\backend\forms\PhotosForm;
use Yii;
use app\modules\backend\forms\MetaForm;
use app\models\Blog;
use app\modules\backend\models\BlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blog();
        $model->meta = new MetaForm();
        $model->imageFile = new PhotosForm();
        if ($model->load(Yii::$app->request->post()) && $model->meta->load(Yii::$app->request->post())) {
            $parent = Blog::findOne($model->parentId);
            $model->appendTo($parent);
            if ($model->imageFile->validate() && $model->imageFile->files) {
                $model->setPhoto($model->imageFile->files[0]);
            }
            $model->save();

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->meta = new MetaForm($model->meta);
        $model->imageFile = new PhotosForm();
        $model->parentId = $model->parent;
        $oldParentid = $model->parentId;

        if ($model->load(Yii::$app->request->post()) && $model->meta->load(Yii::$app->request->post()) && $model->imageFile->load(Yii::$app->request->post())) {

            if ($model->parentId !== $oldParentid) {
                $parent = Blog::findOne($model->parentId);
                $model->appendTo($parent);
                if ($model->imageFile->validate() && $model->imageFile->files) {
                    $model->setPhoto($model->imageFile->files[0]);
                }
                $model->save();
            }
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
