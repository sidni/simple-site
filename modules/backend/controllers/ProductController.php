<?php

namespace app\modules\backend\controllers;

use app\models\Category;
use app\models\CategoryAssignments;
use app\models\Characteristic;
use app\models\CharacteristicValue;
use app\modules\backend\forms\CharacteristicValueForm;
use app\modules\backend\forms\MetaForm;
use app\modules\backend\forms\PhotosForm;
use Yii;
use app\models\Product;
use app\modules\backend\models\ProductSearch;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-photo' => ['POST'],
                    'move-photo-up' => ['POST'],
                    'move-photo-down' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
	    $model->meta = new MetaForm($model->meta);
	    $model->imageFile = new PhotosForm();
	    $model->characteristicForms = array_map(function (Characteristic $characteristic) {
		    return new CharacteristicValueForm($characteristic);
	    }, Characteristic::find()->orderBy('sort')->all());

	    if(Yii::$app->request->isPost){
		    if ($model->imageFile->validate() && $model->imageFile->files) {
			    foreach ($model->imageFile->files as $file){
			        $model->addPhoto($file);
			    }
		    }
	    }
        if ($model->load(Yii::$app->request->post()) && $model->meta->load(Yii::$app->request->post())) {
	        if (Model::loadMultiple($model->characteristicForms, Yii::$app->request->post()) && Model::validateMultiple($model->characteristicForms)) {
		        if(is_array($model->characteristicForms)){
			        $value = [];
			        foreach ($model->characteristicForms as $characteristic){
				        $value[] = ['characteristic_id'=> $characteristic->id,'value'=>$characteristic->value];
			        }
			        $model->characteristicValues = $value;
		        }
	        }
			if(is_array($model->categoryAssignmentsValues)){
				$value = [];
				foreach ($model->categoryAssignmentsValues as $category){
					$value[] = ['category_id'=>$category];
				}
				$model->categoryAssignments = $value;
			}

	        /*if(!$model->validate()){
	        	var_dump($model->getErrors());
	        }/**/
	        $model->save();
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {


	    $model = $this->findModel($id);
	    $model->meta = new MetaForm($model->meta);
	    $model->imageFile = new PhotosForm();
	    $model->characteristicForms = array_map(function (Characteristic $characteristic) use ($model) {
		    return new CharacteristicValueForm($characteristic,$model->getValue($characteristic->id));
	    }, Characteristic::find()->orderBy('sort')->all());
	    $model->categoryAssignmentsValues = ArrayHelper::getColumn($model->categoryAssignments, 'category_id');

	    if(Yii::$app->request->isPost){
		    if ($model->imageFile->validate() && $model->imageFile->files) {
			    foreach ($model->imageFile->files as $file){
				    $model->addPhoto($file);
			    }
		    }
	    }
	    if ($model->load(Yii::$app->request->post()) && $model->meta->load(Yii::$app->request->post())) {
		    if (Model::loadMultiple($model->characteristicForms, Yii::$app->request->post()) && Model::validateMultiple($model->characteristicForms)) {
			    if(is_array($model->characteristicForms)){
				    $value = [];
				    foreach ($model->characteristicForms as $characteristic){
					    $value[] = ['product_id'=>$model->id, 'characteristic_id'=> $characteristic->id,'value'=>$characteristic->value];
				    }
				    if(count($value)>0){
					    //CharacteristicValue::deleteAll(['product_id' => $model->id]);
				    }
				    $model->characteristicValues = $value;
			    }
		    }
		    if(is_array($model->categoryAssignmentsValues)){
			    $value = [];
			    foreach ($model->categoryAssignmentsValues as $category){
				    $value[] = ['product_id'=>$model->id, 'category_id'=>$category];
			    }
			    if(count($value)>0){
			    	//CategoryAssignments::deleteAll(['product_id' => $model->id]);
			    }
			    $model->categoryAssignments = $value;
		    }

		    /*if(!$model->validate()){
				var_dump($model->getErrors());
			}/**/
		    $model->save();
		    return $this->redirect(['update', 'id' => $model->id]);
	    }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	/**
	 * @param integer $id
	 * @param $photoId
	 * @return mixed
	 */
	public function actionMovePhotoUp($id, $photoId)
	{
		$product = $this->findModel($id);
		$product->movePhotoUp($photoId);
		$product->save();

		return $this->redirect(['update', 'id' => $id, '#' => 'photos']);
	}

	/**
	 * @param integer $id
	 * @param $photoId
	 * @return mixed
	 */
	public function actionMovePhotoDown($id, $photoId)
	{
		$product = $this->findModel($id);
		$product->movePhotoDown($photoId);
		$product->save();
		return $this->redirect(['update', 'id' => $id, '#' => 'photos']);
	}

	/**
	 * @param integer $id
	 * @param $photoId
	 * @return mixed
	 */
	public function actionDeletePhoto($id, $photoId)
	{
		try {
			$product = $this->findModel($id);
			$product->removePhoto($photoId);
			$product->save();
		} catch (\DomainException $e) {
			Yii::$app->session->setFlash('error', $e->getMessage());
		}
		return $this->redirect(['update', 'id' => $id, '#' => 'photos']);
	}
}
