<?php

namespace app\modules\backend\controllers;

use app\modules\backend\forms\SliderForm;
use Yii;
use app\models\Slider;
use app\modules\backend\models\SliderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use app\modules\backend\forms\PhotosForm;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($name)
    {
	    $models = $this->findModelByName($name);
	    $sliderName = $this->findSliderName($name);

        return $this->render('view', [
            'model' =>$models,
	        'sliderName' => $sliderName,
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'name' => $model->name]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $name
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($name)
    {
        $models = $this->findModelByName($name);
        foreach ($models as $model){
	        $model->imageFile = new PhotosForm();
	        $model->imageFile->id = $model->id;
        }
	    //$imageFile = new PhotosForm();
	    $newSlide = new Slider();
	    $newSlide->imageFile = new PhotosForm();

		$sliderName = new SliderForm($name);
        if( Yii::$app->request->isPost){
			if($sliderName->load(Yii::$app->request->post()) && $sliderName->validate() && (int)$sliderName->changeTitle === 1){
				Slider::updateAll(['name'=>$sliderName->name],['name'=>$name]);
				return $this->redirect(['update', 'name' => $sliderName->name]);
			}

	        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
		        foreach ($models as $model) {
		        	//TODO need check or set try catch if uploaded images not all slides
			        if ($model->imageFile->validate() && $model->imageFile->files) {
			        	if(isset($model->imageFile->files[0]) && !is_null($model->imageFile->files[0])){
					        $model->setPhoto($model->imageFile->files[0]);
				        }
			        }
			        $model->save();
		        }
	        }
	        if ($newSlide->load(Yii::$app->request->post()) && $newSlide->validate()) {
		        $newSlide->name = $name;
		        if ($newSlide->imageFile->validate() && $newSlide->imageFile->files) {
			        if ( isset( $newSlide->imageFile->files[0] ) && ! is_null( $newSlide->imageFile->files[0] ) ) {
				        $newSlide->setPhoto( $newSlide->imageFile->files[0] );
			        }
		        }
		        $newSlide->save();
	        }
	        return $this->redirect(['update', 'name' => $name]);
        }

        return $this->render('update', [
            'slides' => $models,
	        'newSlide' => $newSlide,
	        'sliderName' => $sliderName,
        ]);
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($name)
    {
	    Slider::deleteAll(['name' => $name]);
        return $this->redirect(['index']);
    }

	/**
	 * Deletes an existing Slider model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDeleteSlide($id)
	{
		$slider = $this->findModel($id);
		if(!is_null($slider)){
			$name = $slider->name;
			$slider->delete();
			return $this->redirect(['update', 'name' => $name]);
		}
		return $this->redirect(['index']);
	}
    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	/**
	 * Finds the Slider model based on its name value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Slider the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModelByName($name)
	{
		if (($model = Slider::find()->where(['name' => $name])->orderBy('id')->indexBy('id')->all()) !== []) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}

	/**
	 * Finds the Slider nam4 model
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Slider the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findSliderName($name)
	{
		if (($model = Slider::find()->select(['name'])->where(['name' => $name])->limit(1)->one()) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
