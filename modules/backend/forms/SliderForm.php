<?php

namespace app\modules\backend\forms;

use yii\base\Model;

class SliderForm extends Model
{
    public $name;
    public $changeTitle = 0;

    public function __construct($name = null, $config = [])
    {
        if ($name) {
            $this->name = $name;
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 50],
	        [['changeTitle'], 'integer'],
        ];
    }
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'name' => 'Название слайдера (Индетификатор)',
			'changeTitle' => 'Флаг смены имени',
		];
	}
}