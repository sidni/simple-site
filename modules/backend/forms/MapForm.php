<?php

namespace app\modules\backend\forms;

use app\models\generic\Meta;
use yii\base\Model;

class MapForm extends Model
{
    public $lat;
    public $lng;
	public $map = null;

    public function __construct($map = null, $config = [])
    {
	    parent::__construct($config);
        if ($map) {
            $this->lat = $map['lat'];
            $this->lng = $map['lng'];
        }
    }

    public function rules()
    {
        return [
            [['lat', 'lng'], 'double'],
        ];
    }

	public function beforeValidate()
	{
		if (parent::beforeValidate()) {
			if(is_numeric($this->lat) && is_numeric($this->lng)){
				$this->map = ['lat'=>$this->lat,'lng'=>$this->lng];
			}
			return true;
		}
		return false;
	}
}