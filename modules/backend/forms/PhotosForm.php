<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 07.01.18
 * Time: 0:24
 */

namespace app\modules\backend\forms;


use yii\base\Model;
use yii\web\UploadedFile;

class PhotosForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;
    public $id = false;
    public function rules()
    {
        return [
            ['files', 'each', 'rule' => ['image']],
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
        	if(is_numeric($this->id)){
		        $this->files = UploadedFile::getInstances($this, 'files['.$this->id.']');
	        }else{
	            $this->files = UploadedFile::getInstances($this, 'files');
	        }
            return true;
        }
        return false;
    }
}