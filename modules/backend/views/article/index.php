<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Article;
use app\widgets\grid\SetColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'value' => function (Article $model) {
                            return $model->photo ? Html::img($model->getThumbFileUrl('photo', 'admin')) : null;
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 100px'],
                    ],
                    'title',
                    [
                        'attribute' => 'blog_id',
                        'filter' => $searchModel->blogsList(),
                        'value' => 'blog.title',
                    ],
                    //'content:ntext',
                    //'status',
                    //'blog_id',
                    'hits',
                    //'created_at',
                    //'updated_at',
                    [
                        'class' => SetColumn::className(),
                        'filter' => Article::getStatusesArray(),
                        'attribute' => 'status',
                        'name' => 'statusName',
                        'cssCLasses' => [
                            Article::STATUS_ACTIVE => 'success',
                            Article::STATUS_DISABLED => 'warning',
                        ],
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
