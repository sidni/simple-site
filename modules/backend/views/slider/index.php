<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать слайдер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
	                ['class' => 'yii\grid\ActionColumn',
	                 'template'=>'{view}{update}{delete}',
	                 'buttons'=>[
		                 'view' => function ($url, $model) {
			                 return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['slider/view', 'name' => $model->name]), [
				                 'title' => 'Просмотреть',
			                 ]);
		                 },
		                 'update' => function ($url, $model) {
			                 return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['slider/update', 'name' => $model->name]), [
				                 'title' => 'Редактировать',
			                 ]);
		                 },
		                 'delete' => function ($url, $model) {

			                 return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['slider/delete', 'name' => $model->name], [
				                 'class' => '',
				                 'title' => 'Удалить',
				                 'data' => [
					                 'confirm' => 'Уверены, что хотите удалить слайдер?',
					                 'method' => 'post',
				                 ],
			                 ]);
		                 },
	                 ]
	                ],
                ],
            ]); ?>
        </div>
    </div>
</div>
