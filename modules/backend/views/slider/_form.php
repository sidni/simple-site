<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $slides app\models\Slider */
/* @var $newSlide app\models\Slider */
/* @var $sliderName app\modules\backend\forms\SliderForm  */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">
	<?php $formTitle = ActiveForm::begin(); ?>
    <div class="box box-default">
        <div class="box-header with-border">
            Основное
        </div>
        <div class="box-body">
	        <?=Html::activeHiddenInput($sliderName, 'changeTitle', ['value' => 1]); ?>
	        <?= $formTitle->field($sliderName, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="box-body">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
	<?php ActiveForm::end(); ?>

	<?php $form = ActiveForm::begin([
		'options' => ['enctype'=>'multipart/form-data']
	]);
	?>

    <div class="row">
	    <?php
        $i=0;
        foreach ($slides as $index => $model):
            $i++;
        ?>

            <div class="col-md-6 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">Слайд <?= $i ?>
                        <div class="box-tools pull-right">
	                        <?= Html::a('<span aria-hidden="true">&times;</span>', ['slider/delete-slide', 'id' => $model->id], [
		                        'class' => 'btn btn-danger',
		                        'data' => [
			                        'confirm' => 'Уверены, что хотите удалить слайдер?',
			                        'method' => 'post',
		                        ],
	                        ]) ?>
                        </div>
                    </div>
                    <div class="box-body">
                            <?=Html::activeHiddenInput($model, '[' . $index . ']id', ['value' => $model->id]); ?>

                            <?= $form->field($model, '[' . $index . ']comment')->widget(CKEditor::className(),[
	                            'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
	                                    'preset' => 'basic',
	                                    'clientOptions' => [
		                                    'config.allowedContent' => true,
		                                    'allowedContent' => true,
	                                    ],
                                ]),
                            ]) ?>

                            <?php if ($model->photo): ?>
	                            <?= Html::a(Html::img($model->getThumbFileUrl('photo', 'thumb'),['style'=>'height:230px']), $model->getUploadedFileUrl('photo'), [
		                            'class' => 'thumbnail',
		                            'target' => '_blank'
	                            ]) ?>
                            <?php endif; ?>
                            <?php echo $form->field($model->imageFile, 'files[' . $model->id . ']')->label(false)->widget(FileInput::classname(), [
                                'options' => ['multiple' => true],
                                'language' => 'ru',
                                'pluginOptions' => ['previewFileType' => 'any']
                            ]); ?>

                            <?= $form->field($model, '[' . $index . ']link')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
	    <?php ActiveForm::end(); ?>
	    <?php $form = ActiveForm::begin([
		    'options' => ['enctype'=>'multipart/form-data']
	    ]);
	    ?>
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">Новый Слайд</div>
                <div class="box-body">
				    <?= $form->field($newSlide, 'comment')->widget(CKEditor::className(),[
					    'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
						    'preset' => 'basic',
						    'clientOptions' => [
							    'config.allowedContent' => true,
							    'allowedContent' => true,
						    ],
					    ]),
				    ]) ?>
				    <?php echo $form->field($newSlide->imageFile, 'files')->label(false)->widget(FileInput::classname(), [
					    'options' => ['multiple' => true],
					    'language' => 'ru',
					    'pluginOptions' => ['previewFileType' => 'any']
				    ]); ?>
				    <?= $form->field($newSlide, 'link')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>


    <?php ActiveForm::end(); ?>

</div>
