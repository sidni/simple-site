<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $slides app\models\Slider */
/* @var $newSlide app\models\Slider */
/* @var $sliderName app\modules\backend\forms\SliderForm  */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box box-default">
        <div class="box-header with-border">
            Основное
        </div>
        <div class="box-body">
	        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">Новый Слайд</div>
                <div class="box-body">

				    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

				    <?= $form->field($model, 'photo')->textInput(['maxlength' => true]) ?>

				    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>


    <?php ActiveForm::end(); ?>

</div>
