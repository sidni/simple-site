<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $sliderName app\models\Slider */


$this->title = $sliderName->name;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'name' => $sliderName->name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'name' => $sliderName->name], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить слайдер?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $sliderName,
                'attributes' => [
                    'name',
                ],
            ]) ?>
        </div>
    </div>
</div>
