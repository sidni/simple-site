<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $slides app\models\Slider */
/* @var $newSlide app\models\Slider */
/* @var $sliderName app\modules\backend\forms\SliderForm */

$this->title = 'Редактировать Слайдер: '.$sliderName->name;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $sliderName->name, 'url' => ['view', 'name' => $sliderName->name]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="slider-update">
    <div class="box-body">
        <?= $this->render('_form', [
            'slides' => $slides,
            'sliderName' => $sliderName,
            'newSlide' => $newSlide,
        ]) ?>
    </div>
</div>
