<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\InfoPage;
use app\widgets\grid\SetColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\InfoPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Информационные страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-page-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    'title',
                    //'top_content:ntext',
                    //'description:ntext',
                    //'status',
                    //'sort',
                    [
                        'class' => SetColumn::className(),
                        'filter' => InfoPage::getStatusesArray(),
                        'attribute' => 'status',
                        'name' => 'statusName',
                        'cssCLasses' => [
                            InfoPage::STATUS_ACTIVE => 'success',
                            InfoPage::STATUS_DISABLED => 'warning',
                        ],
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
