<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InfoPage */

$this->title = 'Редактировать информационную статью: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Инф. страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="info-page-update">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
