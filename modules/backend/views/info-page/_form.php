<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use app\models\InfoPage;
use app\models\Slider;

/* @var $this yii\web\View */
/* @var $model app\models\InfoPage */
/* @var $form yii\widgets\ActiveForm */

use app\assets\MapBackendAsset;
MapBackendAsset::register($this);
?>

<div class="info-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-header with-border">Основные</div>
        <div class="box-body">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'top_content')->widget(CKEditor::className(),[
                'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
	                'preset' => 'standard',
	                'clientOptions' => [
		                'config.allowedContent' => true,
		                'allowedContent' => true,
	                ],
                ]),
            ]) ?>
            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
	                'preset' => 'standard',
	                'clientOptions' => [
		                'config.allowedContent' => true,
		                'allowedContent' => true,
	                ],
                ]),
            ]) ?>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">СЕО</div>
        <div class="box-body">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">Дополнительные</div>
        <div class="box-body">
            <?= $form->field($model, 'status')->dropDownList(InfoPage::getStatusesArray()) ?>
            <?= $form->field($model, 'sort')->textInput() ?>
            <?= $form->field($model, 'slider_name')->dropDownList(Slider::getSliderNames()) ?>
        </div>
    </div>
    <?php if(is_object($model->map)): ?>
        <div class="box box-default">
            <div class="box-header with-border">Карта проезда</div>
            <div class="box-body">
                <div id="map_container">
                    <div id="mapc"></div>
                </div>
                <?= $form->field($model->map, 'lat')->hiddenInput(['id'=>'latitude'])->label(false) ?>
                <?= $form->field($model->map, 'lng')->hiddenInput(['id'=>'longitude'])->label(false) ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    <?php if(is_null($model->map->lng) || is_null($model->map->lat)): ?>
    var centerMap = {lat: 49.9434246, lng: 36.3004635};
    <?php else: ?>
    var centerMap = {lat: <?=$model->map->lat; ?>, lng: <?=$model->map->lng; ?>};
    <?php endif; ?>
</script>