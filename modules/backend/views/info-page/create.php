<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InfoPage */

$this->title = 'Добавить информационную страницу';
$this->params['breadcrumbs'][] = ['label' => 'Info Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-page-create">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
