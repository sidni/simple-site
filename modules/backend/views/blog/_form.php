<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use app\models\Blog;
use kartik\file\FileInput;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]);
    ?>

    <div class="box box-default">
        <div class="box-header with-border">Основные</div>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'content')->widget(CKEditor::className(),[
                'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
	                'preset' => 'standard',
	                'clientOptions' => [
		                'config.allowedContent' => true,
		                'allowedContent' => true,
	                ],
                ]),
            ]) ?>
            <?= $form->field($model, 'parentId')->dropDownList($model->parentBlogList()) ?>

        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">СЕО</div>
        <div class="box-body">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">Фото</div>
        <div class="box-body">
            <?php if ($model->photo): ?>
                <?= Html::a(Html::img($model->getThumbFileUrl('photo', 'thumb')), $model->getUploadedFileUrl('photo'), [
                    'class' => 'thumbnail',
                    'target' => '_blank'
                ]) ?>
            <?php endif; ?>
            <?= $form->field($model->imageFile, 'files')->label(false)->widget(FileInput::className(), [
                'options' => [
                    'accept' => 'image/*',
                ]
            ]) ?>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">Дополнительные</div>
        <div class="box-body">
            <?= $form->field($model, 'status')->dropDownList(Blog::getStatusesArray()) ?>
            <?= $form->field($model, 'sort')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
