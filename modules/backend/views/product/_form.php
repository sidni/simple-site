<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use kartik\widgets\FileInput;
use app\models\Product;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

	<?php $form = ActiveForm::begin([
		'options' => ['enctype'=>'multipart/form-data']
	]);
	?>
    <div class="box box-default">
        <div class="box-header with-border">Общие</div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
					<?= $form->field($model, 'brand_id')->dropDownList($model->brandsList()) ?>
                </div>
                <div class="col-md-2">
					<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
					<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
	        <?= $form->field($model, 'description')->widget(CKEditor::className(),[
		        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
			        'preset' => 'standard',
			        'clientOptions' => [
				        'config.allowedContent' => true,
				        'allowedContent' => true,
			        ],
		        ]),
	        ]) ?>
	        <?= $form->field($model, 'status')->dropDownList(Product::getStatusesArray()) ?>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">Склад</div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
					<?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
					<?= $form->field($model, 'quantity')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">Price</div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
					<?= $form->field($model, 'price_new')->textInput() ?>
                </div>
                <div class="col-md-6">
					<?= $form->field($model, 'price_old')->textInput() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">Категории</div>
                <div class="box-body">
					<?= $form->field($model, 'category_id')->dropDownList($model->categoriesList(), ['prompt' => '']) ?>
					<?= $form->field($model, 'categoryAssignmentsValues')->checkboxList($model->categoriesList()) ?>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">Характеристики</div>
                <div class="box-body">
                    <?php foreach ($model->characteristicForms as $i => $value): ?>
                        <?php if ($variants = $value->variantsList()): ?>
                            <?= $form->field($value, '[' . $i . ']value')->dropDownList($variants, ['prompt' => '']) ?>
                        <?php else: ?>
                            <?= $form->field($value, '[' . $i . ']value')->textInput() ?>
                        <?php endif ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-default">
        <div id="photos" class="box-header with-border">Фото</div>
        <div class="box-body">
	        <?php foreach ($model->photos as $photo): ?>
                <div class="col-md-2 col-xs-3" style="text-align: center">
                    <div class="btn-group">
				        <?= Html::a('<span class="glyphicon glyphicon-arrow-left"></span>', ['move-photo-up', 'id' => $model->id, 'photoId' => $photo->id], [
					        'class' => 'btn btn-default',
					        'data-method' => 'post',
				        ]); ?>
				        <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['delete-photo', 'id' => $model->id, 'photoId' => $photo->id], [
					        'class' => 'btn btn-default',
					        'data-method' => 'post',
					        'data-confirm' => 'Remove photo?',
				        ]); ?>
				        <?= Html::a('<span class="glyphicon glyphicon-arrow-right"></span>', ['move-photo-down', 'id' => $model->id, 'photoId' => $photo->id], [
					        'class' => 'btn btn-default',
					        'data-method' => 'post',
				        ]); ?>
                    </div>
                    <div>
				        <?= Html::a(
					        Html::img($photo->getThumbFileUrl('photo', 'thumb')),
					        $photo->getUploadedFileUrl('photo'),
					        ['class' => 'thumbnail', 'target' => '_blank']
				        ) ?>
                    </div>
                    <div>
	                    <?= $form->field($model, 'main_photo_id')->radio(['label' => '', 'value' => $photo->id, 'uncheck' => null]) ?>
                    </div>
                </div>
	        <?php endforeach; ?>

	        <?= $form->field($model->imageFile, 'files[]')->label(false)->widget(FileInput::className(), [
		        'options' => [
			        'accept' => 'image/*',
			        'multiple' => true,
		        ],
		        'language' => 'ru',
		        'pluginOptions' => ['previewFileType' => 'any']
	        ]) ?>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">СЕО</div>
        <div class="box-body">
			<?= $form->field($model->meta, 'title')->textInput() ?>
			<?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
			<?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>








    <?= $form->field($model, 'rating')->textInput(['maxlength' => true]) ?>




    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
