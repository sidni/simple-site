<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Product;
use app\widgets\grid\SetColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить продукт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
	                [
		                'value' => function (Product $model) {
			                return $model->main_photo_id ? Html::img($model->mainPhoto->getThumbFileUrl('photo', 'admin')) : null;
		                },
		                'format' => 'raw',
		                'contentOptions' => ['style' => 'width: 100px'],
	                ],
	                'code',
	                'name',
	                [
		                'attribute' => 'category_id',
		                'filter' => $searchModel->categoriesList(),
		                'value' => 'category.name',
	                ],
	                [
		                'class' => SetColumn::className(),
		                'filter' => Product::getStatusesArray(),
		                'attribute' => 'status',
		                'name' => 'statusName',
		                'cssCLasses' => [
			                Product::STATUS_ACTIVE => 'success',
			                Product::STATUS_DRAFT => 'warning',
		                ],
	                ],
	                [
		                'attribute' => 'brand_id',
		                'filter' => $searchModel->brandsList(),
		                'value' => 'brand.name',
	                ],
                    //'description:ntext',
                    //'price_old',
                    //'price_new',
                    //'rating',
                    //'meta_json',
                    //'main_photo_id',
                    //'status',
                    //'weight',
                    //'quantity',
                    //'created_at',
                    //'updated_at',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
