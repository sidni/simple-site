<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить продукт?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    //'category_id',
                    //'brand_id',
                    'code',
                    'name',
                    //'description:ntext',
                    'price_old',
                    'price_new',
                    'rating',
                    //'meta_json',
                    //'main_photo_id',
                    //'status',
                    'weight',
                    'quantity',
                    //'created_at',
                    //'updated_at',
                ],
            ]) ?>
        </div>
    </div>
</div>
