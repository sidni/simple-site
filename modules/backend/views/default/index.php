<?php

use app\modules\admin\Module;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\user\models\backend\User */

$this->title = 'Административная панель';
?>
<div class="admin-default-index">

    <p>
        <?= Html::a('Пользователи', ['user/default'], ['class' => 'btn btn-primary']) ?>
    </p>
</div>
