<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-header with-border">Основные</div>
        <div class="box-body">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
	                'preset' => 'standard',
	                'clientOptions' => [
		                'config.allowedContent' => true,
		                'allowedContent' => true,
	                ],
                ]),
            ]) ?>
            <?= $form->field($model, 'parentId')->dropDownList($model->parentCategoriesList()) ?>
            <?= $form->field($model, 'status')->dropDownList(Category::getStatusesArray()) ?>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">Фото</div>
        <div class="box-body">
            <?php if ($model->photo): ?>
                <?= Html::a(Html::img($model->getThumbFileUrl('photo', 'thumb')), $model->getUploadedFileUrl('photo'), [
                    'class' => 'thumbnail',
                    'target' => '_blank'
                ]) ?>
            <?php endif; ?>
            <?= $form->field($model->imageFile, 'files')->label(false)->widget(FileInput::className(), [
                'options' => [
                    'accept' => 'image/*',
                ]
            ]) ?>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">СЕО</div>
        <div class="box-body">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">Дополнительные</div>
        <div class="box-body">
            <?= $form->field($model, 'on_main')->dropDownList(Category::getStatusesDisplayArray()) ?>
            <?= $form->field($model, 'sort')->textInput() ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>