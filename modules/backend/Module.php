<?php
/**
 * Created by PhpStorm.
 * User: sid
 * Date: 21.12.17
 * Time: 23:18
 */


namespace app\modules\backend;

use app\modules\backend\rbac\Rbac;
use yii\filters\AccessControl;
use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\backend\controllers';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Rbac::PERMISSION_ADMIN_PANEL],
                    ],
                ],
            ],
        ];
    }
}